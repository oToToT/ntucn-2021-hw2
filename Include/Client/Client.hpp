#pragma once

#include <filesystem>
#include <string>
#include <vector>

class Client {
  int ConnFd;
  std::filesystem::path Root;
  void sendMessage(uint8_t *Buf, size_t Count);
  void recvMessage(uint8_t *Buf, size_t Count);

public:
  Client(int ClientId, std::string Ip, uint16_t Port);
  std::vector<std::string> listFiles();
  void uploadFiles(const std::vector<std::string> &Files);
  void downloadFiles(const std::vector<std::string> &Files);
  void playVideo(std::string Filename);
};

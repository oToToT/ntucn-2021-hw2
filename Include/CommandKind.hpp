#pragma once

enum CommandKind {
  CK_LIST,
  CK_UPLOAD,
  CK_DOWNLOAD,
  CK_PLAY,
};

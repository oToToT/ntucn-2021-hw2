#pragma once

#include "Server/ServerConnection.hpp"
#include <list>
#include <memory>

class Server {
private:
  int ListenerFd;
  int FdLimit;

  virtual void newConnection(int Fd) = 0;

protected:
  std::list<std::unique_ptr<ServerConnection>> Connections;

public:
  Server(uint16_t Port);
  virtual ~Server() = default;
  void Run();
};

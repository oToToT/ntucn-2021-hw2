#pragma once

#include <cstddef>
#include <cstdint>

class ServerConnection {
private:
  int Fd;

  virtual void finishRecving() = 0;
  virtual void finishSending() = 0;

protected:
  uint8_t *Buf;
  size_t BufOffset, BufSize;

public:
  ServerConnection(int _Fd);
  virtual ~ServerConnection();
  int getFd() const { return Fd; }

  enum ConnectionStateKind {
    CSK_RECVING,
    CSK_SENDING,
    CSK_SCHEDULING,
  };

  virtual ConnectionStateKind getState() = 0;
  bool sendMessage();
  bool recvMessage();
  virtual void doSchedule() = 0;
};

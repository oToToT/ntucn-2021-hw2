#pragma once

#include "CommandKind.hpp"
#include "Server/ServerConnection.hpp"
#include <filesystem>
#include <functional>

class FileServerConnection : public ServerConnection {
private:
  std::filesystem::path Root;
  virtual void finishRecving() final;
  virtual void finishSending() final;

  void prepareToReadCommand() {
    State = ICSK_READ_COMMAND;

    if (Buf != nullptr) {
      delete[] Buf;
    }

    BufSize = sizeof(CommandKind);
    Buf = new uint8_t[BufSize];
    BufOffset = 0;
  }

  enum InternalConnectionStateKind {
    ICSK_READ_COMMAND,
    ICSK_LIST,

    ICSK_DOWNLOAD_FILENAME_LENGTH,
    ICSK_DOWNLOAD_FILENAME,
    ICSK_DOWNLOAD_CONTENT_LENGTH,
    ICSK_DOWNLOAD_READ_CONTENT,
    ICSK_DOWNLOAD,

    ICSK_UPLOAD_FILENAME_LENGTH,
    ICSK_UPLOAD_FILENAME,
    ICSK_UPLOAD_CONTENT_LENGTH,
    ICSK_UPLOAD_READ_CONTENT,
    ICSK_UPLOAD_SAVE_CONTENT,
    ICSK_UPLOAD,

    ICSK_PLAY_FILENAME_LENGTH,
    ICSK_PLAY_FILENAME,
    ICSK_PLAY_VIDEO_INFO,
    ICSK_PLAY_NEED_FRAME,
    ICSK_PLAY_SEND_FRAME,
    ICSK_PLAY,
  } State;

  void *Store;
  std::function<void()> CleanupHelper;

  void startList();
  void finishList();

  void startDownload();
  void finishDownloadFilenameLength();
  void finishDownloadFilename();
  void finishDownloadContentLength();
  void finishDownloadReadContent();
  void finishDownload();

  void startUpload();
  void finishUploadFilenameLength();
  void finishUploadFilename();
  void finishUploadContentLength();
  void finishUploadReadContent();
  void finishUploadSaveContent();
  void finishUpload();

  void startPlay();
  void finishPlayFilenameLength();
  void finishPlayFilename();
  void finishPlayVideoInfo();
  void finishPlayNeedFrame();
  void finishPlay();

public:
  FileServerConnection(std::filesystem::path _Root, int Fd);
  virtual ~FileServerConnection();
  virtual ConnectionStateKind getState() final;
  virtual void doSchedule() final;
};

#pragma once

#include "Server/Server.hpp"
#include <filesystem>

class FileServer : public Server {
private:
  std::filesystem::path Root;

  virtual void newConnection(int Fd) final;

public:
  FileServer(uint16_t Port, std::filesystem::path _Root);
  virtual ~FileServer() = default;
};

#include "Server/Server.hpp"
#include <algorithm>
#include <arpa/inet.h>
#include <cstring>
#include <iostream>
#include <sys/resource.h>

Server::Server(uint16_t Port) {
  ListenerFd = socket(AF_INET, SOCK_STREAM, 0);
  if (ListenerFd < 0) {
    std::cout << "Cannot create socket" << std::endl;
    exit(0);
  }
  const int Yes = 1;
  if (setsockopt(ListenerFd, SOL_SOCKET, SO_REUSEADDR, (void *)&Yes,
                 sizeof(Yes)) < 0) {
    std::cout << "Cannot set socket option" << std::endl;
    exit(0);
  }

  timeval MaxWaitTime = {.tv_sec = 0, .tv_usec = 0};
  if (setsockopt(ListenerFd, SOL_SOCKET, SO_RCVTIMEO, &MaxWaitTime,
                 sizeof(MaxWaitTime)) < 0) {
    std::cout << "Cannot set max weight time of socket" << std::endl;
    exit(0);
  }

  sockaddr_in ServerAddr;
  memset(&ServerAddr, 0, sizeof(ServerAddr));
  ServerAddr.sin_family = AF_INET;
  ServerAddr.sin_addr.s_addr = htonl(INADDR_ANY);
  ServerAddr.sin_port = htons(Port);

  if (bind(ListenerFd, reinterpret_cast<sockaddr *>(&ServerAddr),
           sizeof(ServerAddr)) < 0) {
    std::cout << "Cannot bind" << std::endl;
    exit(0);
  }

  if (listen(ListenerFd, /*backlog=*/1024) < 0) {
    std::cout << "Cannot listen" << std::endl;
    exit(0);
  }

  rlimit FdRLimit;
  if (getrlimit(RLIMIT_NOFILE, &FdRLimit) < 0) {
    std::cout << "Cannot get fd limit" << std::endl;
    exit(0);
  }
  FdLimit = std::min<int>(FdRLimit.rlim_cur, FD_SETSIZE);
}

void Server::Run() {
  timeval TimeOut = {0, 0};
  fd_set ReadableFds, WritableFds, ExceptionalFds;
  FD_ZERO(&ReadableFds);
  FD_ZERO(&WritableFds);
  FD_ZERO(&ExceptionalFds);
  while (true) {
    // Handle existing connections
    for (auto &Connection : Connections) {
      auto Fd = Connection->getFd();
      switch (Connection->getState()) {
      case ServerConnection::CSK_RECVING:
        FD_SET(Fd, &ReadableFds);
        FD_SET(Fd, &ExceptionalFds);
        break;
      case ServerConnection::CSK_SENDING:
        FD_SET(Fd, &WritableFds);
        FD_SET(Fd, &ExceptionalFds);
        break;
      case ServerConnection::CSK_SCHEDULING:
        // nothing to check
        break;
      }
    }
    FD_SET(ListenerFd, &ReadableFds);

    auto Selected =
        select(FdLimit, &ReadableFds, &WritableFds, &ExceptionalFds, &TimeOut);
    if (Selected < 0) {
      if (errno == EINTR or errno == EAGAIN) {
        continue;
      }
      std::cout << "Cannot select" << std::endl;
      exit(0);
    }

    for (auto I = Connections.begin(), E = Connections.end(); I != E; ++I) {
      auto &Connection = *I;
      const int Fd = Connection->getFd();
      if (FD_ISSET(Fd, &ExceptionalFds)) {
        I = std::prev(Connections.erase(I));
        FD_CLR(Fd, &ReadableFds);
      } else if (FD_ISSET(Fd, &ReadableFds)) {
        if (not Connection->recvMessage()) {
          I = std::prev(Connections.erase(I));
        }
        FD_CLR(Fd, &ReadableFds);
      } else if (FD_ISSET(Fd, &WritableFds)) {
        if (not Connection->sendMessage()) {
          I = std::prev(Connections.erase(I));
        }
        FD_CLR(Fd, &WritableFds);
      } else if (Connection->getState() == ServerConnection::CSK_SCHEDULING) {
        Connection->doSchedule();
      }
    }

    if (FD_ISSET(ListenerFd, &ReadableFds) == 0) {
      // No new connection
      continue;
    }

    sockaddr_in NewConnection;
    auto SocketSize = sizeof(NewConnection);
    int NewFd = accept(ListenerFd, reinterpret_cast<sockaddr *>(&NewConnection),
                       reinterpret_cast<socklen_t *>(&SocketSize));
    if (NewFd < 0) {
      if (errno == EINTR or errno == EAGAIN) {
        continue;
      }
      std::cout << "Cannot accept new connection" << std::endl;
      exit(0);
    }

    newConnection(NewFd);
  }
}

#include "Server/FileServer.hpp"
#include "Server/FileServerConnection.hpp"
#include <iostream>
#include <memory>

FileServer::FileServer(uint16_t Port, std::filesystem::path _Root)
    : Server(Port), Root(_Root) {
  std::cout << "Starting File Server @ Port = " << Port << ", Root = " << Root
            << std::endl;
  std::filesystem::create_directory(Root);
}

void FileServer::newConnection(int Fd) {
  auto Connection = std::make_unique<FileServerConnection>(Root, Fd);
  Connections.emplace_back(std::move(Connection));
}

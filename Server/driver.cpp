#include "Server/FileServer.hpp"
#include <iostream>

int main(int Argc, const char *Argv[]) {
  if (Argc != 2) {
    const char *ProgName = "./server";
    if (Argc > 0) {
      ProgName = Argv[0];
    }
    std::cout << "Usage: " << ProgName << " [PORT]" << std::endl;
    return 0;
  }

  const int Port = std::stoi(Argv[1]);
  // TODO: handle exception, 0 <= Port < 65536

  FileServer Server(Port, "b08902100_server_folder");
  Server.Run();

  return 0;
}

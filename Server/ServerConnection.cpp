#include "Server/ServerConnection.hpp"
#include <algorithm>
#include <cstddef>
#include <sys/socket.h>
#include <unistd.h>

ServerConnection::ServerConnection(int Fd_) : Fd(Fd_), Buf(nullptr) {}

ServerConnection::~ServerConnection() {
  if (Buf != nullptr) {
    delete[] Buf;
  }
  close(Fd);
}

bool ServerConnection::recvMessage() {
  size_t ReadLimit = std::min<size_t>(BufSize - BufOffset, 1024);
  ssize_t ReadCount = recv(Fd, Buf + BufOffset, ReadLimit, MSG_DONTWAIT);
  if (ReadCount == 0) {
    return false;
  }
  if (ReadCount < 0) {
    if (errno == EAGAIN or errno == EWOULDBLOCK) {
      return true;
    }
    return false;
  }
  BufOffset += static_cast<size_t>(ReadCount);

  if (BufOffset == BufSize) {
    finishRecving();
  }
  return true;
}

bool ServerConnection::sendMessage() {
  size_t WriteLimit = std::min<size_t>(BufSize - BufOffset, 1024);
  ssize_t WriteCount =
      send(Fd, Buf + BufOffset, WriteLimit, MSG_NOSIGNAL | MSG_DONTWAIT);
  if (WriteCount < 0) {
    if (errno == EAGAIN or errno == EWOULDBLOCK) {
      return true;
    }
    return false;
  }
  BufOffset += static_cast<size_t>(WriteCount);

  if (BufOffset == BufSize) {
    finishSending();
  }
  return true;
}

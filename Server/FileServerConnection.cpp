#include "Server/FileServerConnection.hpp"
#include <iostream>

FileServerConnection::FileServerConnection(std::filesystem::path _Root, int _Fd)
    : ServerConnection(_Fd), Root(_Root), Store(nullptr) {

  prepareToReadCommand();

  std::cout << "New File Server Connection with Fd = " << getFd() << std::endl;
}

FileServerConnection::~FileServerConnection() {
  if (CleanupHelper) {
    CleanupHelper();
  }
}

void FileServerConnection::finishRecving() { doSchedule(); }

void FileServerConnection::finishSending() { doSchedule(); }

ServerConnection::ConnectionStateKind FileServerConnection::getState() {
  switch (State) {
  case ICSK_READ_COMMAND:
    return ServerConnection::CSK_RECVING;
  case ICSK_LIST:
    return ServerConnection::CSK_SENDING;

  case ICSK_DOWNLOAD_FILENAME_LENGTH:
    return ServerConnection::CSK_RECVING;
  case ICSK_DOWNLOAD_FILENAME:
    return ServerConnection::CSK_RECVING;
  case ICSK_DOWNLOAD_CONTENT_LENGTH:
    return ServerConnection::CSK_SENDING;
  case ICSK_DOWNLOAD_READ_CONTENT:
    return ServerConnection::CSK_SCHEDULING;
  case ICSK_DOWNLOAD:
    return ServerConnection::CSK_SENDING;

  case ICSK_UPLOAD_FILENAME_LENGTH:
    return ServerConnection::CSK_RECVING;
  case ICSK_UPLOAD_FILENAME:
    return ServerConnection::CSK_RECVING;
  case ICSK_UPLOAD_CONTENT_LENGTH:
    return ServerConnection::CSK_RECVING;
  case ICSK_UPLOAD_READ_CONTENT:
    return ServerConnection::CSK_RECVING;
  case ICSK_UPLOAD_SAVE_CONTENT:
    return ServerConnection::CSK_SCHEDULING;
  case ICSK_UPLOAD:
    return ServerConnection::CSK_SCHEDULING;

  case ICSK_PLAY_FILENAME_LENGTH:
    return ServerConnection::CSK_RECVING;
  case ICSK_PLAY_FILENAME:
    return ServerConnection::CSK_RECVING;
  case ICSK_PLAY_VIDEO_INFO:
    return ServerConnection::CSK_SENDING;
  case ICSK_PLAY_NEED_FRAME:
    return ServerConnection::CSK_RECVING;
  case ICSK_PLAY_SEND_FRAME:
    return ServerConnection::CSK_SENDING;
  case ICSK_PLAY:
    return ServerConnection::CSK_SCHEDULING;
  }
}

void FileServerConnection::doSchedule() {
  switch (State) {
  case ICSK_READ_COMMAND: {
    CommandKind Command = *reinterpret_cast<CommandKind *>(Buf);
    delete[] Buf;
    Buf = nullptr;

    switch (Command) {
    case CK_LIST:
      startList();
      break;
    case CK_UPLOAD:
      startUpload();
      break;
    case CK_DOWNLOAD:
      startDownload();
      break;
    case CK_PLAY:
      startPlay();
      break;
    }
    break;
  }
  case ICSK_LIST:
    finishList();
    break;

  case ICSK_DOWNLOAD_FILENAME_LENGTH:
    finishDownloadFilenameLength();
    break;
  case ICSK_DOWNLOAD_FILENAME:
    finishDownloadFilename();
    break;
  case ICSK_DOWNLOAD_CONTENT_LENGTH:
    finishDownloadContentLength();
    break;
  case ICSK_DOWNLOAD_READ_CONTENT:
    finishDownloadReadContent();
    break;
  case ICSK_DOWNLOAD:
    finishDownload();
    break;

  case ICSK_UPLOAD_FILENAME_LENGTH:
    finishUploadFilenameLength();
    break;
  case ICSK_UPLOAD_FILENAME:
    finishUploadFilename();
    break;
  case ICSK_UPLOAD_CONTENT_LENGTH:
    finishUploadContentLength();
    break;
  case ICSK_UPLOAD_READ_CONTENT:
    finishUploadReadContent();
    break;
  case ICSK_UPLOAD_SAVE_CONTENT:
    finishUploadSaveContent();
    break;
  case ICSK_UPLOAD:
    finishUpload();
    break;

  case ICSK_PLAY_FILENAME_LENGTH:
    finishPlayFilenameLength();
    break;
  case ICSK_PLAY_FILENAME:
    finishPlayFilename();
    break;
  case ICSK_PLAY_VIDEO_INFO:
    finishPlayVideoInfo();
    break;
  case ICSK_PLAY_NEED_FRAME:
    finishPlayNeedFrame();
    break;
  case ICSK_PLAY_SEND_FRAME:
    finishPlayVideoInfo();
    break;
  case ICSK_PLAY:
    finishPlay();
    break;
  }
}

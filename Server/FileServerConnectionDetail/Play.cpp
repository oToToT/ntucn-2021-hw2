#include "Server/FileServerConnection.hpp"
#include "opencv2/opencv.hpp"

void FileServerConnection::startPlay() {
  State = ICSK_PLAY_FILENAME_LENGTH;

  BufSize = sizeof(size_t);
  Buf = new uint8_t[BufSize];
  BufOffset = 0;
}

void FileServerConnection::finishPlayFilenameLength() {
  State = ICSK_PLAY_FILENAME;

  size_t FilenameLength = *reinterpret_cast<size_t *>(Buf);
  delete[] Buf;

  Buf = new uint8_t[FilenameLength];
  BufOffset = 0;
  BufSize = FilenameLength;
}

static_assert(sizeof(double) == sizeof(size_t), "Incompatible platform");

struct VideoToWrite {
  cv::VideoCapture Video;
  cv::Mat Frame;
};

void FileServerConnection::finishPlayFilename() {
  State = ICSK_PLAY_VIDEO_INFO;

  std::string Filename(Buf, Buf + BufSize);
  delete[] Buf;

  std::filesystem::path File = Root / Filename;
  size_t Exists = std::filesystem::exists(File);

  if (Exists == 1 and File.extension() != ".mpg") {
    Exists = 2;
  }

  if (Exists == 1) {

    Store = new VideoToWrite;
    auto &VideoInfo = *static_cast<VideoToWrite *>(Store);
    VideoInfo.Video = cv::VideoCapture(File.c_str());

    CleanupHelper = [this] { delete static_cast<VideoToWrite *>(Store); };

    size_t Height, Width;
    Height = VideoInfo.Video.get(cv::CAP_PROP_FRAME_HEIGHT);
    Width = VideoInfo.Video.get(cv::CAP_PROP_FRAME_WIDTH);

    VideoInfo.Frame = cv::Mat::zeros(Height, Width, CV_8UC3);
    if (not VideoInfo.Frame.isContinuous()) {
      VideoInfo.Frame = VideoInfo.Frame.clone();
    }

    BufSize = sizeof(size_t) * 5;
    Buf = new uint8_t[BufSize];
    BufOffset = 0;

    uint8_t *BufPtr = Buf;
    *reinterpret_cast<size_t *>(BufPtr) = Exists;
    BufPtr += sizeof(size_t);
    *reinterpret_cast<size_t *>(BufPtr) = Height;
    BufPtr += sizeof(size_t);
    *reinterpret_cast<size_t *>(BufPtr) = Width;
    BufPtr += sizeof(size_t);
    *reinterpret_cast<double *>(BufPtr) = VideoInfo.Video.get(cv::CAP_PROP_FPS);
    BufPtr += sizeof(double);
    *reinterpret_cast<double *>(BufPtr) =
        VideoInfo.Video.get(cv::CAP_PROP_FRAME_COUNT);
    BufPtr += sizeof(size_t);
  } else {
    BufSize = sizeof(size_t);
    Buf = new uint8_t[BufSize];
    BufOffset = 0;
    *reinterpret_cast<size_t *>(Buf) = Exists;
  }
}

void FileServerConnection::finishPlayVideoInfo() {
  State = ICSK_PLAY_NEED_FRAME;

  delete[] Buf;
  Buf = nullptr;

  if (Store == nullptr) {
    State = ICSK_PLAY;
    return;
  }

  BufSize = sizeof(bool);
  Buf = new uint8_t[BufSize];
  BufOffset = 0;
}

void FileServerConnection::finishPlayNeedFrame() {
  State = ICSK_PLAY_SEND_FRAME;

  bool Bye;
  Bye = *reinterpret_cast<bool *>(Buf);
  delete[] Buf;
  Buf = nullptr;

  auto &VideoInfo = *static_cast<VideoToWrite *>(Store);
  VideoInfo.Video >> VideoInfo.Frame;

  if (Bye or VideoInfo.Frame.empty()) {
    VideoInfo.Video.release();
    VideoInfo.Frame.release();
    delete static_cast<VideoToWrite *>(Store);
    Store = nullptr;
    CleanupHelper = nullptr;

    State = ICSK_PLAY;
  } else {
    BufSize = VideoInfo.Frame.total() * VideoInfo.Frame.elemSize();
    Buf = new uint8_t[BufSize];
    memcpy(Buf, VideoInfo.Frame.data, BufSize);
    BufOffset = 0;
  }
}

void FileServerConnection::finishPlay() { prepareToReadCommand(); }

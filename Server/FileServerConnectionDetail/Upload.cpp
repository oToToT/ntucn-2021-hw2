#include "Server/FileServerConnection.hpp"
#include <fcntl.h>
#include <unistd.h>

void FileServerConnection::startUpload() {
  State = ICSK_UPLOAD_FILENAME_LENGTH;

  BufSize = sizeof(size_t);
  Buf = new uint8_t[BufSize];
  BufOffset = 0;
}

void FileServerConnection::finishUploadFilenameLength() {
  State = ICSK_UPLOAD_FILENAME;

  size_t FilenameLength = *reinterpret_cast<size_t *>(Buf);
  delete[] Buf;

  Buf = new uint8_t[FilenameLength];
  BufOffset = 0;
  BufSize = FilenameLength;
}

void FileServerConnection::finishUploadFilename() {
  State = ICSK_UPLOAD_CONTENT_LENGTH;

  Store = new std::string(Buf, Buf + BufSize);
  CleanupHelper = [this] { delete static_cast<std::string *>(Store); };
  delete[] Buf;

  BufSize = sizeof(size_t);
  Buf = new uint8_t[BufSize];
  BufOffset = 0;
}

void FileServerConnection::finishUploadContentLength() {
  State = ICSK_UPLOAD_READ_CONTENT;

  size_t FileSize = *reinterpret_cast<size_t *>(Buf);
  delete[] Buf;

  BufSize = FileSize;
  Buf = new uint8_t[BufSize];
  BufOffset = 0;
}

struct FileContentToWrite {
  int FileDescriptor;
  uint8_t *FileContent;
  size_t FileSize;
};

void FileServerConnection::finishUploadReadContent() {
  State = ICSK_UPLOAD_SAVE_CONTENT;

  std::string Filename = *static_cast<std::string *>(Store);
  delete static_cast<std::string *>(Store);
  Store = new FileContentToWrite;
  CleanupHelper = [this] { delete static_cast<FileContentToWrite *>(Store); };

  FileContentToWrite &SavedContent = *static_cast<FileContentToWrite *>(Store);

  auto FilePath = Root / Filename;
  int FileDescriptor = open(FilePath.c_str(), O_WRONLY | O_CREAT | O_TRUNC,
                            S_IRUSR | S_IWUSR | S_IRGRP | S_IROTH);
  if (FileDescriptor < 0) {
    // TODO: error handling
  }
  SavedContent.FileDescriptor = FileDescriptor;
  SavedContent.FileContent = Buf;
  SavedContent.FileSize = BufSize;
}

void FileServerConnection::finishUploadSaveContent() {
  FileContentToWrite &SavedContent = *static_cast<FileContentToWrite *>(Store);

  if (SavedContent.FileSize > 0) {
    size_t WriteLimit = std::min<size_t>(SavedContent.FileSize, 1024);
    auto WriteCount = write(SavedContent.FileDescriptor,
                            SavedContent.FileContent, WriteLimit);
    if (WriteCount < 0) {
      // TODO: error handling
    }
    SavedContent.FileContent += WriteCount;
    SavedContent.FileSize -= WriteCount;
  } else {
    State = ICSK_UPLOAD;
  }
}

void FileServerConnection::finishUpload() {
  FileContentToWrite &SavedContent = *static_cast<FileContentToWrite *>(Store);
  close(SavedContent.FileDescriptor);
  delete static_cast<FileContentToWrite *>(Store);
  Store = nullptr;
  CleanupHelper = nullptr;

  prepareToReadCommand();
}

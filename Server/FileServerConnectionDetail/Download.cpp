#include "Server/FileServerConnection.hpp"
#include <cstring>
#include <fcntl.h>
#include <iostream>
#include <unistd.h>

void FileServerConnection::startDownload() {
  State = ICSK_DOWNLOAD_FILENAME_LENGTH;

  BufSize = sizeof(size_t);
  Buf = new uint8_t[BufSize];
  BufOffset = 0;
}

void FileServerConnection::finishDownloadFilenameLength() {
  State = ICSK_DOWNLOAD_FILENAME;

  size_t FilenameLength = *reinterpret_cast<size_t *>(Buf);
  delete[] Buf;

  Buf = new uint8_t[FilenameLength];
  BufOffset = 0;
  BufSize = FilenameLength;
}

struct FileContentToRead {
  int FileDescriptor;
  uint8_t *FileContent;
  size_t FileSize;
};

void FileServerConnection::finishDownloadFilename() {
  State = ICSK_DOWNLOAD_CONTENT_LENGTH;

  std::string Filename(Buf, Buf + BufSize);
  delete[] Buf;

  std::filesystem::path File = Root / Filename;
  bool Exists = std::filesystem::exists(File);

  if (Exists) {
    size_t FileSize = std::filesystem::file_size(File);
    BufSize = sizeof(size_t) + sizeof(size_t);
    Buf = new uint8_t[BufSize];
    BufOffset = 0;
    uint8_t *BufPtr = Buf;
    *reinterpret_cast<size_t *>(BufPtr) = Exists;
    BufPtr += sizeof(size_t);
    *reinterpret_cast<size_t *>(BufPtr) = FileSize;

    int FileDescriptor = open(File.c_str(), O_RDONLY);
    if (FileDescriptor < 0) {
      // TODO: error handling
    }

    Store = new FileContentToRead;
    CleanupHelper = [this] { delete static_cast<FileContentToRead *>(Store); };
    FileContentToRead &SavedContent = *static_cast<FileContentToRead *>(Store);
    SavedContent.FileDescriptor = FileDescriptor;
    SavedContent.FileContent = nullptr;
    SavedContent.FileSize = FileSize;
  } else {
    BufSize = sizeof(size_t);
    Buf = new uint8_t[BufSize];
    BufOffset = 0;
    *reinterpret_cast<size_t *>(Buf) = Exists;

    State = ICSK_DOWNLOAD;
  }
}

void FileServerConnection::finishDownloadContentLength() {
  delete[] Buf;
  State = ICSK_DOWNLOAD_READ_CONTENT;

  FileContentToRead &SavedContent = *static_cast<FileContentToRead *>(Store);
  BufSize = SavedContent.FileSize;
  Buf = new uint8_t[BufSize];
  BufOffset = 0;
  SavedContent.FileContent = Buf;
}

void FileServerConnection::finishDownloadReadContent() {
  FileContentToRead &SavedContent = *static_cast<FileContentToRead *>(Store);

  if (SavedContent.FileSize > 0) {
    size_t ReadLimit = std::min<size_t>(SavedContent.FileSize, 1024);
    auto ReadCount =
        read(SavedContent.FileDescriptor, SavedContent.FileContent, ReadLimit);
    if (ReadCount < 0) {
      // TODO: error handling
    }
    SavedContent.FileContent += ReadCount;
    SavedContent.FileSize -= ReadCount;
  } else {
    State = ICSK_DOWNLOAD;
  }
}

void FileServerConnection::finishDownload() {
  if (Store != nullptr) {
    FileContentToRead &SavedContent = *static_cast<FileContentToRead *>(Store);
    close(SavedContent.FileDescriptor);
    delete static_cast<FileContentToRead *>(Store);
    Store = nullptr;
    CleanupHelper = nullptr;
  }

  prepareToReadCommand();
}

#include "Server/FileServerConnection.hpp"
#include <cstring>
#include <vector>

void FileServerConnection::startList() {
  State = ICSK_LIST;

  std::filesystem::directory_iterator FileList(Root);
  std::vector<std::string> Files;
  BufSize = sizeof(size_t);
  for (const auto &File : FileList) {
    Files.push_back(File.path().filename());
    BufSize += sizeof(size_t) + Files.back().size();
  }
  Buf = new uint8_t[BufSize];
  BufOffset = 0;

  uint8_t *Result = Buf;
  *reinterpret_cast<size_t *>(Result) = Files.size();
  Result += sizeof(size_t);
  for (const auto &File : Files) {
    *reinterpret_cast<size_t *>(Result) = File.size();
    Result += sizeof(size_t);
  }
  for (const auto &File : Files) {
    memcpy(Result, File.data(), File.size());
    Result += File.size();
  }
}

void FileServerConnection::finishList() { prepareToReadCommand(); }

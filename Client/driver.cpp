#include "Client/Client.hpp"
#include "ErrorMessage.hpp"
#include "Lib/StringView.hpp"
#include <algorithm>
#include <cctype>
#include <iostream>
#include <sstream>

int main(int Argc, const char *Argv[]) {
  if (Argc != 3) {
    const char *ProgName = "./server";
    if (Argc > 0) {
      ProgName = Argv[0];
    }
    std::cout << "Usage: " << ProgName << " [client_id] [ip]:[port]"
              << std::endl;
  }

  const int ClientId = std::stoi(Argv[1]);

  const std::string_view IpPort = Argv[2];
  const auto SeperatorPos = IpPort.find(":");
  const auto Ip = std::string(IpPort.substr(0, SeperatorPos));
  const auto Port = std::stoi(IpPort.substr(SeperatorPos + 1).data());

  Client Client(ClientId, Ip, Port);

  while (true) {
    std::string Command;
    std::cin >> Command;

    std::string CommandArgsStr;
    std::getline(std::cin, CommandArgsStr);

    std::string CommandArgs(trimSpace(CommandArgsStr));

    if (Command == "ls") {
      if (not CommandArgs.empty()) {
        std::cout << INVALID_CMD_FORMAT_STR << std::endl;
        continue;
      }

      auto Files = Client.listFiles();

      for (auto File : Files) {
        std::cout << File << std::endl;
      }
    } else if (Command == "put") {
      std::stringstream CommandArgParser(CommandArgs);

      std::vector<std::string> Files;
      while (true) {
        std::string File;
        if (not(CommandArgParser >> File)) {
          break;
        }
        Files.push_back(File);
      }

      Client.uploadFiles(Files);
    } else if (Command == "get") {
      std::stringstream CommandArgParser(CommandArgs);

      std::vector<std::string> Files;
      while (true) {
        std::string File;
        if (not(CommandArgParser >> File)) {
          break;
        }
        Files.push_back(File);
      }

      Client.downloadFiles(Files);
    } else if (Command == "play") {
      auto SpaceIt =
          std::find_if(CommandArgs.begin(), CommandArgs.end(),
                       [](unsigned char c) { return std::isspace(c); });
      if (SpaceIt != CommandArgs.end()) {
        std::cout << INVALID_CMD_FORMAT_STR << std::endl;
        continue;
      }

      Client.playVideo(CommandArgs);
    } else {
      std::cout << INVALID_CMD_FORMAT_STR << std::endl;
    }
  }
  return 0;
}

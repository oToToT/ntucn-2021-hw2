#include "Client/Client.hpp"
#include "CommandKind.hpp"
#include "opencv2/opencv.hpp"
#include <arpa/inet.h>
#include <chrono>
#include <fcntl.h>
#include <iostream>
#include <sys/sendfile.h>
#include <unistd.h>

void Client::sendMessage(uint8_t *Buf, size_t Count) {
  while (Count > 0) {
    auto WriteCount = write(ConnFd, Buf, Count);
    if (WriteCount < 0) {
      std::cout << "Bad write" << std::endl;
      exit(0);
    } else {
      Buf += WriteCount;
      Count -= WriteCount;
    }
  }
}

void Client::recvMessage(uint8_t *Buf, size_t Count) {
  while (Count > 0) {
    auto ReadCount = read(ConnFd, Buf, Count);
    if (ReadCount < 0) {
      std::cout << "Bad read" << std::endl;
      exit(0);
    } else {
      Buf += ReadCount;
      Count -= ReadCount;
    }
  }
}

Client::Client(int ClientId, std::string Ip, uint16_t Port) {
  ConnFd = socket(AF_INET, SOCK_STREAM, 0);
  if (ConnFd < 0) {
    std::cout << "Cannot create socket" << std::endl;
    exit(0);
  }

  timeval MaxWaitTime = {.tv_sec = 0, .tv_usec = 0};
  setsockopt(ConnFd, SOL_SOCKET, SO_RCVTIMEO, &MaxWaitTime,
             sizeof(MaxWaitTime));

  sockaddr_in ServerAddr;
  memset(&ServerAddr, 0, sizeof(ServerAddr));
  ServerAddr.sin_family = AF_INET;
  ServerAddr.sin_addr.s_addr = inet_addr(Ip.data());
  ServerAddr.sin_port = htons(Port);

  if (connect(ConnFd, reinterpret_cast<sockaddr *>(&ServerAddr),
              sizeof(ServerAddr)) < 0) {
    std::cout << "Cannot connect to " << Ip.data() << ":" << uint32_t(Port)
              << std::endl;
    exit(0);
  }

  Root = "b08902100_" + std::to_string(ClientId) + "_client_folder";
  std::filesystem::create_directory(Root);
}

std::vector<std::string> Client::listFiles() {
  CommandKind Command = CK_LIST;
  sendMessage(reinterpret_cast<uint8_t *>(&Command), sizeof(Command));
  size_t FileCount;
  recvMessage(reinterpret_cast<uint8_t *>(&FileCount), sizeof(FileCount));
  std::vector<std::string> Files(FileCount);
  for (auto &File : Files) {
    size_t FileLength;
    recvMessage(reinterpret_cast<uint8_t *>(&FileLength), sizeof(FileLength));
    File.assign(FileLength + 1, '\0');
  }
  for (auto &File : Files) {
    recvMessage(reinterpret_cast<uint8_t *>(File.data()), File.size() - 1);
  }
  return Files;
}

void Client::uploadFiles(const std::vector<std::string> &Files) {
  for (auto File : Files) {
    auto FilePath = Root / File;
    if (not std::filesystem::is_regular_file(FilePath) or
        not std::filesystem::exists(FilePath)) {
      std::cout << "The " << File << " doesn't exist." << std::endl;
      continue;
    }

    std::cout << "putting " << File << "......" << std::endl;

    CommandKind Command = CK_UPLOAD;
    sendMessage(reinterpret_cast<uint8_t *>(&Command), sizeof(Command));

    uint8_t FilenameInfo[sizeof(size_t) + File.size()];
    *reinterpret_cast<size_t *>(FilenameInfo) = File.size();
    memcpy(FilenameInfo + sizeof(size_t), File.data(), File.size());
    sendMessage(FilenameInfo, sizeof(FilenameInfo));

    size_t FileSize = std::filesystem::file_size(FilePath);
    sendMessage(reinterpret_cast<uint8_t *>(&FileSize), sizeof(FileSize));

    uint8_t *FileContentPtr = new uint8_t[FileSize];
    uint8_t *FileContent = FileContentPtr;
    size_t SizeLeft = FileSize;

    int FileDescriptor = open(FilePath.c_str(), O_RDONLY);
    while (SizeLeft > 0) {
      auto ReadCount = read(FileDescriptor, FileContent, SizeLeft);
      if (ReadCount < 0) {
        std::cout << "Error while reading " << FilePath << std::endl;
        exit(0);
      } else {
        SizeLeft -= ReadCount;
        FileContent += ReadCount;
      }
    }
    close(FileDescriptor);

    sendMessage(FileContentPtr, FileSize);

    delete[] FileContentPtr;
  }
}

void Client::downloadFiles(const std::vector<std::string> &Files) {
  for (auto File : Files) {
    CommandKind Command = CK_DOWNLOAD;
    sendMessage(reinterpret_cast<uint8_t *>(&Command), sizeof(Command));

    uint8_t *FilenameInfo = new uint8_t[sizeof(size_t) + File.size()];
    *reinterpret_cast<size_t *>(FilenameInfo) = File.size();
    memcpy(FilenameInfo + sizeof(size_t), File.data(), File.size());
    sendMessage(FilenameInfo, sizeof(size_t) + File.size());
    delete[] FilenameInfo;

    size_t Ok;
    recvMessage(reinterpret_cast<uint8_t *>(&Ok), sizeof(Ok));
    if (not Ok) {
      std::cout << "The " << File << " doesn't exist." << std::endl;
      continue;
    }

    std::cout << "getting " << File << "......" << std::endl;

    size_t FileSize;
    recvMessage(reinterpret_cast<uint8_t *>(&FileSize), sizeof(FileSize));

    uint8_t *FileContent = new uint8_t[FileSize];
    recvMessage(FileContent, FileSize);

    auto FilePath = Root / File;
    int FileDescriptor = open(FilePath.c_str(), O_WRONLY | O_CREAT | O_TRUNC,
                              S_IRUSR | S_IWUSR | S_IRGRP | S_IROTH);
    if (FileDescriptor < 0) {
      std::cout << "Cannot open " << FilePath << std::endl;
      exit(0);
    }
    uint8_t *FileContentPtr = FileContent;
    while (FileSize > 0) {
      auto WriteCount = write(FileDescriptor, FileContent, FileSize);
      if (WriteCount < 0) {
        std::cout << "Cannot write to " << FilePath << std::endl;
        exit(0);
      }
      FileContent += WriteCount;
      FileSize -= WriteCount;
    }
    close(FileDescriptor);
    delete[] FileContentPtr;
  }
}

void Client::playVideo(std::string Filename) {
  CommandKind Command = CK_PLAY;
  sendMessage(reinterpret_cast<uint8_t *>(&Command), sizeof(Command));

  size_t FilenameLength = Filename.size();
  sendMessage(reinterpret_cast<uint8_t *>(&FilenameLength),
              sizeof(FilenameLength));

  sendMessage(reinterpret_cast<uint8_t *>(Filename.data()), FilenameLength);

  size_t Found;
  recvMessage(reinterpret_cast<uint8_t *>(&Found), sizeof(Found));
  if (Found == 0) {
    std::cout << "The " << Filename << " doesn't exists." << std::endl;
    return;
  }
  if (Found == 2) {
    std::cout << "The " << Filename << " is not a mpg file." << std::endl;
    return;
  }
  std::cout << "playing the video..." << std::endl;

  size_t Height, Width;
  recvMessage(reinterpret_cast<uint8_t *>(&Height), sizeof(Height));
  recvMessage(reinterpret_cast<uint8_t *>(&Width), sizeof(Width));

  double Fps;
  recvMessage(reinterpret_cast<uint8_t *>(&Fps), sizeof(Fps));

  size_t FrameCount;
  recvMessage(reinterpret_cast<uint8_t *>(&FrameCount), sizeof(FrameCount));

  cv::Mat Frame = cv::Mat::zeros(Height, Width, CV_8UC3);
  if (not Frame.isContinuous()) {
    // Make Frame continuous
    Frame = Frame.clone();
  }

  bool Bye = false;

  for (size_t i = 0; i < FrameCount; ++i) {
    auto StartTime = std::chrono::high_resolution_clock::now();

    sendMessage(reinterpret_cast<uint8_t *>(&Bye), sizeof(Bye));
    if (Bye) {
      break;
    }
    recvMessage(reinterpret_cast<uint8_t *>(Frame.data),
                Frame.total() * Frame.elemSize());
    imshow("Video", Frame);

    auto EndTime = std::chrono::high_resolution_clock::now();
    std::chrono::duration<double> ElapsedTime = EndTime - StartTime;

    int WaitTime = std::max<int>(1, (1 / Fps - ElapsedTime.count()) * 1000);

    if (cv::waitKey(WaitTime) == 27) {
      Bye = true;
    }
  }

  cv::destroyAllWindows();
}

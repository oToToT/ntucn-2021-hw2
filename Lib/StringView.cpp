#include "Lib/StringView.hpp"
#include <algorithm>
#include <cctype>

std::string_view trimSpace(std::string_view Str) {
  auto TrimmedBegin = std::find_if_not(
      Str.begin(), Str.end(), [](unsigned char c) { return std::isspace(c); });
  Str.remove_prefix(std::distance(Str.begin(), TrimmedBegin));
  auto TrimmedEnd =
      std::find_if_not(Str.rbegin(), Str.rend(),
                       [](unsigned char c) { return std::isspace(c); });
  Str.remove_suffix(std::distance(Str.rbegin(), TrimmedEnd));
  return Str;
}

.PHONY: clean all

CXX = g++
CXXFLAGS = -Ofast -march=native -flto -std=c++17 -fuse-ld=gold -Wall -Wextra -Wshadow -Wconversion -Wwrite-strings
CXXLIBS = -IInclude `pkg-config --cflags --libs opencv`

AR ?= ar
ARFLAGS = rD

all: server client

server: Server/driver.cpp Server/Server.cpp Server/ServerConnection.cpp Server/FileServer.cpp Server/FileServerConnection.cpp libfileserverconnection.a Include/Server/Server.hpp Include/Server/FileServer.hpp
	$(CXX) $(CXXFLAGS) $(CXXLIBS) -o server Server/driver.cpp Server/Server.cpp Server/ServerConnection.cpp Server/FileServer.cpp Server/FileServerConnection.cpp libfileserverconnection.a

client: Client/driver.cpp Client/Client.cpp Include/Client/Client.hpp lib.o
	$(CXX) $(CXXFLAGS) $(CXXLIBS) -o client Client/driver.cpp Client/Client.cpp lib.o

lib.o: Lib/StringView.cpp Include/Lib/StringView.hpp
	$(CXX) $(CXXFLAGS) $(CXXLIBS) -o lib.o -c Lib/StringView.cpp


FILESERVERCONNECTIONDETAILLIBS = Include/Server/ServerConnection.hpp Include/Server/FileServerConnection.hpp

fsc-upload.o: Server/FileServerConnectionDetail/Upload.cpp $(FILESERVERCONNECTIONDETAILLIBS)
	$(CXX) $(CXXFLAGS) $(CXXLIBS) -o fsc-upload.o -c Server/FileServerConnectionDetail/Upload.cpp

fsc-download.o: Server/FileServerConnectionDetail/Download.cpp $(FILESERVERCONNECTIONDETAILLIBS)
	$(CXX) $(CXXFLAGS) $(CXXLIBS) -o fsc-download.o -c Server/FileServerConnectionDetail/Download.cpp

fsc-list.o: Server/FileServerConnectionDetail/List.cpp $(FILESERVERCONNECTIONDETAILLIBS)
	$(CXX) $(CXXFLAGS) $(CXXLIBS) -o fsc-list.o -c Server/FileServerConnectionDetail/List.cpp

fsc-play.o: Server/FileServerConnectionDetail/Play.cpp $(FILESERVERCONNECTIONDETAILLIBS)
	$(CXX) $(CXXFLAGS) $(CXXLIBS) -o fsc-play.o -c Server/FileServerConnectionDetail/Play.cpp

libfileserverconnection.a: fsc-upload.o fsc-download.o fsc-list.o fsc-play.o
	$(AR) $(ARFLAGS) libfileserverconnection.a fsc-upload.o fsc-download.o fsc-list.o fsc-play.o


clean:
	rm -rf server client lib.o fsc-upload.o fsc-download.o fsc-list.o fsc-play.o libfileserverconnection.a
